const state = () => ({
  videoFullScreen: false,
  tShirtVideoOpened: false,
  gatePassed: false,
  withLandingReferrer: false
})

const mutations = {
  setVideoFullScreen (state, flag) {
    state.videoFullScreen = flag
  },
  setTShirtVideoOpened (state, flag) {
    state.tShirtVideoOpened = flag
  },
  passGate (state) {
    state.gatePassed = true
  },
  setLandingReferrer (state) {
    state.withLandingReferrer = true
  }
}

const actions = {
  nuxtClientInit ({ commit }, { route }) {
    if (route.query.ref === '1') commit('setLandingReferrer')
  }
}

export default {
  actions,
  state,
  mutations
}
