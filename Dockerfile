FROM node:13
WORKDIR /app
RUN npm install pushstate-server -g

COPY package*.json /app/

RUN npm ci
COPY . /app

RUN npm run generate
CMD pushstate-server -d ./dist -p 80
