const DOMAIN = window.location.hostname.split('.').splice(-2).join('.')
const expireDate = (new Date()).setDate((new Date()).getDate() + 365)

const set = (key, value) => {
  document.cookie = `${key}=${JSON.stringify(value)}; domain=${DOMAIN}; expires=${expireDate}; path=/`
}

const get = key => {
  const name = `${key}=`
  const decodedCookie = decodeURIComponent(document.cookie)
  const ca = decodedCookie.split(';')

  for (let i = 0; i < ca.length; i++) {
    let c = ca[i]

    while (c.charAt(0) === ' ') {
      c = c.substring(1)
    }

    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length)
    }
  }

  return ''
}

export default { set, get }
