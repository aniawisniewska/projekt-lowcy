/**
 * Loads asynchronously an image
 *
 * @param src {string}
 * @returns {Promise<string>}
 */
const loadImage = src => (
  new Promise((resolve, reject) => {
    const img = new Image()
    if (img.decode) {
      img.src = src
      img.decode()
        .then(() => resolve(img))
        .catch(() => reject(img))
    } else {
      img.addEventListener('load', () => resolve(img))
      img.addEventListener('error', () => reject(img))
      img.src = src
    }
  })
)

/**
 * Loads asynchronously list of images
 *
 * @param images {Array<string>}
 * @returns {Promise<string>}
 */
const loadImages = (images = []) => Promise.all(images.map(loadImage))

export { loadImage, loadImages }
