require('dotenv').config()

const title = 'Projekt Łowcy 2020'
const description = 'Niezależne nagrody klubowe'

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: description
      },
      { hid: 'og:title', name: 'og:title', content: title },
      { hid: 'og:description', name: 'og:description', content: description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', sizes: '120x120', href: '/apple-icon-120x120.png' },
      { rel: 'icon', sizes: '96x96', href: '/favicon-96x96.png' },
      { href: 'https://cloud.typenetwork.com/projects/4322/fontface.css/', rel: 'stylesheet' }
    ],
    script: [
      {
        src: 'https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
        'data-pace-options': '{ "ajax": false, "restartOnPushState": false, "restartOnRequestAfter": false }'
      },
      {
        src: 'https://cdnjs.cloudflare.com/ajax/libs/smooth-scrollbar/8.4.0/smooth-scrollbar.js',
        defer: true
      }
    ]
  },

  /*
  ** Global CSS
  */
  css: [
    '@/assets/sass/_reset.scss',
    '@/assets/sass/_main.scss',
    '@/assets/sass/_pace.scss'
  ],

  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },

  /*
  ** Modules
  */
  modules: [
    // Doc: https://github.com/SparingSoftware/nuxt-sparing-center
    ['@sparing-software/nuxt-sparing-center', {
      plugins: ['vue-on-resize', '100vh'],
      trailingSlash: true,
      baseImport: true
    }],
    // Doc: https://github.com/nuxt-community/gtm-module
    ...(process.env.GTM_ID
      ? [['@nuxtjs/gtm', { id: process.env.GTM_ID }]]
      : []
    ),

    // Doc: https://github.com/nuxt-community/google-gtag
    ...(process.env.GTAG_ID
      ? [['@nuxtjs/google-gtag', { id: process.env.GTAG_ID }]]
      : []
    )
  ],

  /*
  ** Style resources module configuration
  */
  styleResources: {
    scss: [
      '@/assets/sass/_mixins.scss',
      '@/assets/sass/_vars.scss'
    ]
  },

  /*
  ** Plugins
  */
  plugins: [
    { src: '~plugins/nuxt-client-init', ssr: false }
  ],

  render: {
    static: {
      maxAge: 1000 * 60 * 60 * 24 * 7
    }
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

